# Coding Tests (Java)

*Collection of notable Java coding tests to be hired by great companies*


## Authors
Giacomo Marciani, [gmarciani@acm.org](mailto:gmarciani@acm.org)


## License
The project is released under the [MIT License](https://opensource.org/licenses/MIT).
